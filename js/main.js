let statusx
let statusArry
let boxpoint=[] //Almacena todos los puntos 
let desicion=[]
let IAdesition=[]
let snfile 
let contenido 
let resultado 
let ip = '172.24.139.2' 
let port = '80'

let fullimage = document.getElementById('CanvasFHD')
let fullimagectx = fullimage.getContext('2d')

let recortito= document.getElementById('Canvascut')
let recortitoctx = recortito.getContext('2d')

let horicortito= document.getElementById('Canvascut2')
let horicortitoctx = horicortito.getContext('2d')



//Canvas para tesseract 

let tesser= document.getElementById('tesser') // Recorte 
let tesserctx = tesser.getContext('2d')

let tesser2= document.getElementById('tesser2')
let tesser2ctx = tesser2.getContext('2d')

let tesser3= document.getElementById('tesser3')
let tesser3ctx = tesser3.getContext('2d')



let do1= document.getElementById('DO1') // Canvas donde se encuentra el area a revisar
let do1ctx = do1.getContext('2d')

let do1c2= document.getElementById('DO1c2') // Canvas donde se encuentra el area a revisar
let do1c2tx = do1c2.getContext('2d')




//Canvas para cuadrante 1 
let recorte1= document.getElementById('Cuadrante1')
let recorte1ctx = recorte1.getContext('2d')

//Canvas para cuadrante 2
let recorte2= document.getElementById('Cuadrante2')
let recorte2ctx = recorte2.getContext('2d')

//Canvas para cuadrante 3
let recorte3= document.getElementById('Cuadrante3')
let recorte3ctx = recorte3.getContext('2d')

//Canvas para cuadrante 4
let recorte4= document.getElementById('Cuadrante4')
let recorte4ctx = recorte4.getContext('2d')

//Canvas para cuadrante 5
let recorte5= document.getElementById('Cuadrante5')
let recorte5ctx = recorte5.getContext('2d')

//Canvas para cuadrante 6
let recorte6= document.getElementById('Cuadrante6')
let recorte6ctx = recorte6.getContext('2d')

//Canvas para cuadrante 7
let recorte7= document.getElementById('Cuadrante7')
let recorte7ctx = recorte7.getContext('2d')


//************************************************************************************** Secuencia de prueba */
/*************Secuencia   */ 
async function Sequence(){
    await valorinput() // Se manda llamar funcion para tener un tipo de dato al generar la carpeta 
    //await st(cadenadedatos)

    boxpoint=[] // Reinicia valor para retrabajar punto 
    desicion=[]
    for (point=1; point<9; point++){  
    await arduino(point,ip,port)  
    await open_cam(point)
    canbughide()
    await captureimage()
    await recorta(point)
   // await stopcam()
    } // Cierre de for puntos
    await evaluaArray() // funcion se coloca fuera de for para evaluar toda la cadena
    await response(boxpoint)
    //setTimeout(function fire(){location.reload()},3000);// temporizador para limpiar pantalla
    
}

/******************************************************** Obtener serial  */
 function valorinput(){ // funciona que guarda el elemento de input 
    return new Promise(async resolve =>{ 
    contenido = document.getElementById('myinput').value // Se almacena en una variable
    console.log( typeof (contenido) )
    resolve('resolved')})
 }


//************************************************************************************** Funciones de procesamiento de imagenes */
async function recorta(point){ // Recorte de canvas que se utilizara como muestra para la IA
    return new Promise(async resolve =>{ 
        switch(point){
            case 1:
                //TX1
                recortitoctx.drawImage(fullimage,146,208,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height) // coordenada y tamaño de recorte en el canvas 
                recorte1ctx.drawImage(fullimage,112, 114, 1757, 957, 0, 0, 220, 140) // Ajuste de imagen Cropping y resize
                boxShadow(point) // agrega un sombreado a la imagen 
                await mlinspector(1) // se mnada llamar a la IA y se declara punto especifico a inspeccionar    
                allpoints(1,statusx) //Guarda punto en un array y un estatus

                //IN1
                recortitoctx.drawImage(fullimage,468,207,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height) // coordenada y tamaño de recorte en el canvas 
                boxShadow(point)
                await mlinspector(2)
                allpoints(2,statusx)
                
                //TX2
                recortitoctx.drawImage(fullimage,1195,178,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height) // coordenada y tamaño de recorte en el canvas 
                await mlinspector(3)
                allpoints(3,statusx)

                //TX2
                recortitoctx.drawImage(fullimage,1521,178,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height) // coordenada y tamaño de recorte en el canvas 
                boxShadow(point)
                await mlinspector(4)
                allpoints(4,statusx)
                await snapshot(point)
                break
            case 2:
                // 
                recortitoctx.drawImage(fullimage,299,145,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                recorte2ctx.drawImage(fullimage, 239,28, 1435, 939, 0, 0, 220, 140) // Ajuste de imagen Cropping y resize
                boxShadow(point)
                await mlinspector(5)
                allpoints(5,statusx)
                
                recortitoctx.drawImage(fullimage,624,100,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                await mlinspector(6)
                allpoints(6,statusx)
                await snapshot(point)
                break
            case 3: 
                // 
                recortitoctx.drawImage(fullimage,291,131,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                recorte3ctx.drawImage(fullimage, 227, 71, 1469, 935, 0, 0, 220, 140)
                boxShadow(point)
                await mlinspector(7)
                allpoints(7,statusx)

                recortitoctx.drawImage(fullimage,621,133,316,857,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                boxShadow(point)
                await mlinspector(8)
                allpoints(8,statusx)
                await snapshot(point)
                break
            case 4: 
                // 
                horicortitoctx.drawImage(fullimage,39,10,1023,228,0,0, horicortitoctx.canvas.width, horicortitoctx.canvas.height)
                recorte4ctx.drawImage(fullimage, 19, 1, 1321, 979, 0, 0, 220, 140)
                boxShadow(point)
                await mlinspector(9)
                allpoints(9,statusx)

                horicortitoctx.drawImage(fullimage,59,290,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(10)
                allpoints(10,statusx)

                horicortitoctx.drawImage(fullimage,70,539,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(11)
                allpoints(11,statusx)

                horicortitoctx.drawImage(fullimage,67,774,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(12)
                allpoints(12,statusx)
                await snapshot(point)
                break
            case 5:
                //
                horicortitoctx.drawImage(fullimage,13,10,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                recorte5ctx.drawImage(fullimage, 5, 1, 1119, 1041, 0, 0, 220, 140)
                boxShadow(point)
                await mlinspector(13)
                allpoints(13,statusx)

                horicortitoctx.drawImage(fullimage,18,254,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(14)
                allpoints(14,statusx)

                horicortitoctx.drawImage(fullimage,18,500,1023,228,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(15)
                allpoints(15,statusx)

                horicortitoctx.drawImage(fullimage,18,749,951,253,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
                await mlinspector(16)
                allpoints(16,statusx)
                await snapshot(point)
                break 
            case 6: 
                //
                recortitoctx.drawImage(fullimage,1277,245,645,657,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                recorte6ctx.drawImage(fullimage,1034, 325, 527, 572, 0, 0,  220, 140)
                boxShadow(point)
                await mlinspector(17)
                allpoints(17,statusx)
                await snapshot(point)
                break
            case 7: 
                // 
                recortitoctx.drawImage(fullimage,1277,245,645,657,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                recorte7ctx.drawImage(fullimage, 1034, 325, 527, 572, 0, 0, 220, 140)               
                boxShadow(point)
                await mlinspector(18)
                allpoints(18,statusx)
                await snapshot(point)
                break
                default:
        }
        resolve('resolved')})
}



//************************************************************************************** Funciones tesseract  */

function resultesseract(point){ // Funcion que revisa todo el proceso del recorte para los caracteres 

    switch(point){
        case 1: // TX01 Se revisan caracteres 
            //canvasmove(point)
            analiza(tesser2,1) //Analiza el recorte 
            teseracto(point)
        break
        case 2:
            //IN1 Se revisa por tono de color no por caracter 
            break
        case 3: //TX02 Se revisan caracteres 
            analiza(tesser2,3)
            break
        case 4:
            //IN1 Se revisa por tono de color no por caracter 
            break
        case 5://DO1
            analiza(tesser2,5)
            break
        case 6://DO2
            analiza(tesser2,6)
            break
        case 7://TXI1
            analiza(tesser2,7)
            break
        case 8:
            analiza(tesser2,8)
            break
        case 9:
            analiza(tesser3,9)
            break 
        case 10:
            analiza(tesser3,10)
            break 
        default:
    }

}


function canvasmove(poin){

    //TX01
    if(poin == 1){
        tesserctx.drawImage(fullimage,222,587,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    //TX02
    if(poin == 3){
        tesserctx.drawImage(fullimage,1264,600,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    //DO1
    if(poin == 5){
        tesserctx.drawImage(fullimage,365,221,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    //DO2
    if(poin == 6){
        tesserctx.drawImage(fullimage,676,208,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    // TXI1
    if(poin == 7){
        tesserctx.drawImage(fullimage,355,600,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    // TXI2
    if(poin == 8){
        tesserctx.drawImage(fullimage,691,563,156,229,0,0,tesserctx.canvas.width,tesserctx.canvas.height) // Canvas donde con imagen vertical original 

        tesser2ctx.translate(229,0) // 
        tesser2ctx.rotate(90 * Math.PI / 180)
        tesser2ctx.drawImage(tesser,0,0,200,250,0,0,tesser2ctx.canvas.width,tesser2ctx.canvas.height)
    }
    // TX1
    if(poin == 9){
        tesser3ctx.drawImage(fullimage,606,576,274,142,0,0,tesser3ctx.canvas.width,tesser3ctx.canvas.height) // Canvas donde con imagen vertical original 
    }
    // TX2
    if(poin == 10){
        tesser3ctx.drawImage(fullimage,595,833,274,142,0,0,tesser3ctx.canvas.width,tesser3ctx.canvas.height) // Canvas donde con imagen vertical original 
    }
   
}


function analiza(canvasx,point){// Funcion Analiza especifica para canvas de teseracto
    return new Promise(async resolve =>{
        // canvasx = cuadri 
        let contextcanvasx = canvasx.getContext( '2d' )
         let cdata = contextcanvasx.getImageData(0,0,canvasx.width, canvasx.height);
         //console.log(cdata.data)// ver la matriz de pixeles de la imagen 
         //console.log(cdata.data.length) // ver cantidad total de pixeles 
         let malo=0 , bueno=0
         //Valores del rojo
         let rmin
         let rmax
         //Valores del verde
         let gmin
         let gmax
         //Valores del azul
         let bmin
         let bmax

            // Puntos de cuadrante 1 
         if (point == 1){rmin=0, rmax=146, gmin=0, gmax= 153, bmin=0, bmax=155} // Tonos exactos para que sea leido por teseracto 
         if (point == 3){rmin=0, rmax=146, gmin=0, gmax= 153, bmin=0, bmax=155} // Fibra se inspeccionara por color 
         
          // Puntos de cuadrante 2 
         if (point == 5){rmin=0, rmax=188, gmin=0, gmax= 191, bmin=0, bmax=181}
         if (point == 6){rmin=0, rmax=188, gmin=0, gmax= 191, bmin=0, bmax=181} 

         // Puntos de cuadrante 3 
         if (point == 7){rmin=0, rmax=146, gmin=0, gmax= 153, bmin=0, bmax=155}
         if (point == 8){rmin=0, rmax=146, gmin=0, gmax= 153, bmin=0, bmax=155}

        // Puntos de cuadrante 4
         if (point == 9){rmin=0, rmax=146, gmin=0, gmax= 153, bmin=0, bmax=155}
         if (point == 10){rmin=0, rmax=48, gmin=0, gmax= 86, bmin=0, bmax=71}
         
         // Puntos de cuadrante 5
         if (point == 11){rmin=0, rmax=48, gmin=0, gmax= 86, bmin=0, bmax=71}
         if (point == 12){rmin=0, rmax=48, gmin=0, gmax= 86, bmin=0, bmax=71}




         for (let i = 0; i <cdata.data.length; i += 4) { //cdata.data.length
            // Matriz para valores 
            R = cdata.data[i + 0]
            G = cdata.data[i + 1] 
            B = cdata.data[i + 2]
            A = cdata.data[i + 3]
            //console.log(`Pixn: ${ i / 4 }:-->`, R,G,B,A)
        
          if(((R > rmin) && (R < rmax )) && ((G > gmin) && (G < gmax )) && ((B > bmin) && (B < bmax ))){// condicion para verificar cada pixel
                bueno++
          } else {
                 malo++  // Matriz que pinta de color rojo sino se cumple la condicion anterior 
              cdata.data[i + 0] = 255
              cdata.data[i + 1] = 255
              cdata.data[i + 2] = 255
              cdata.data[i + 3] = 255
          }//End Else
        }// End For
        contextcanvasx.putImageData(cdata,0,0)// Dibuja los pixeles rojos encontrados


    resolve('resolved')})
}

//************************************************************************************** Funciones para dibujar cuadros de status */

function pointstatus(cuadrante,statusx){ // funcion que pinta verde o rojo cuadro analizado 
    
    if((cuadrante == 1)&&(statusx == '1')) {cuad1green();} 
    if((cuadrante == 2)&&(statusx == '1')) {cuad2green();}
    if((cuadrante == 3)&&(statusx == '1')) {cuad3green()}
    if((cuadrante == 6)&&(statusx == '1')) {cuad4green()}
    if((cuadrante == 5)&&(statusx == '1')) {cuad5green()}
    if((cuadrante == 4)&&(statusx == '1')) {cuad6green()}
    if((cuadrante == 7)&&(statusx == '1')) {cuad7green()}
    if((cuadrante == 1)&&(statusx == '0')) {cuad1red();}
    if((cuadrante == 2)&&(statusx == '0')) {cuad2red();}
    if((cuadrante == 3)&&(statusx == '0')) {cuad3red()}
    if((cuadrante == 6)&&(statusx == '0')) {cuad4red()}
    if((cuadrante == 5)&&(statusx == '0')) {cuad5red()}
    if((cuadrante == 4)&&(statusx == '0')) {cuad6red()}
    if((cuadrante == 7)&&(statusx == '0')) {cuad7red()}
}


function boxShadow(point){
    if(point == 1){
    document.getElementById("Cuadrante1").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 2){
    document.getElementById("Cuadrante2").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 3){
    document.getElementById("Cuadrante3").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 4){
        document.getElementById("Cuadrante4").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 5){
        document.getElementById("Cuadrante5").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 6){
        document.getElementById("Cuadrante6").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 7){
        document.getElementById("Cuadrante7").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
}

function cuad1green(){
    let recorte1= document.getElementById('Cuadrante1')
    let recorte1ctx = recorte1.getContext('2d')


    recorte1ctx.strokeStyle = "#39FF14";
    recorte1ctx.lineWidth = 5;
    recorte1ctx.strokeRect(0, 0, 220, 140)
}

function cuad2green(){
    let recorte2= document.getElementById('Cuadrante2')
    let recorte2ctx = recorte2.getContext('2d')


    recorte2ctx.strokeStyle = "#39FF14";
    recorte2ctx.lineWidth = 5;
    recorte2ctx.strokeRect(0, 0, 220, 140)
}

function cuad3green(){
    let recorte3= document.getElementById('Cuadrante3')
    let recorte3ctx = recorte3.getContext('2d')

    recorte3ctx.strokeStyle = "#39FF14";
    recorte3ctx.lineWidth = 5;
    recorte3ctx.strokeRect(0, 0, 220, 140)
}

function cuad4green(){
    let recorte4= document.getElementById('Cuadrante4')
    let recorte4ctx = recorte4.getContext('2d')

    recorte4ctx.strokeStyle = "#39FF14";
    recorte4ctx.lineWidth = 5;
    recorte4ctx.strokeRect(0, 0, 220, 140)
}

function cuad5green(){
    let recorte5= document.getElementById('Cuadrante5')
    let recorte5ctx = recorte5.getContext('2d')

    recorte5ctx.strokeStyle = "#39FF14";
    recorte5ctx.lineWidth = 5;
    recorte5ctx.strokeRect(0, 0, 220, 140)
}

function cuad6green(){
    let recorte6= document.getElementById('Cuadrante6')
    let recorte6ctx = recorte6.getContext('2d')

    recorte6ctx.strokeStyle = "#39FF14";
    recorte6ctx.lineWidth = 5;
    recorte6ctx.strokeRect(0, 0, 220, 140)
}

function cuad7green(){
    let recorte7= document.getElementById('Cuadrante7')
    let recorte7ctx = recorte7.getContext('2d')

    recorte7ctx.strokeStyle = "#39FF14";
    recorte7ctx.lineWidth = 5;
    recorte7ctx.strokeRect(0, 0, 220, 140)
}

// Recuadros color rojo 

function cuad1red(){
    let recorte1= document.getElementById('Cuadrante1')
    let recorte1ctx = recorte1.getContext('2d')

    recorte1ctx.strokeStyle = "#ff0000";
    recorte1ctx.lineWidth = 4;
    recorte1ctx.strokeRect(0, 0, 220, 140)
}

function cuad2red(){
    let recorte2= document.getElementById('Cuadrante2')
    let recorte2ctx = recorte2.getContext('2d')


    recorte2ctx.strokeStyle = "#ff0000";
    recorte2ctx.lineWidth = 4;
    recorte2ctx.strokeRect(0, 0, 220, 140)
}

function cuad3red(){
    let recorte3= document.getElementById('Cuadrante3')
    let recorte3ctx = recorte3.getContext('2d')

    recorte3ctx.strokeStyle = "#ff0000";
    recorte3ctx.lineWidth = 4;
    recorte3ctx.strokeRect(0, 0, 220, 140)
}

function cuad4red(){
    let recorte4= document.getElementById('Cuadrante4')
    let recorte4ctx = recorte4.getContext('2d')

    recorte4ctx.strokeStyle = "#ff0000";
    recorte4ctx.lineWidth = 4;
    recorte4ctx.strokeRect(0, 0, 220, 140)
}

function cuad5red(){
    let recorte5= document.getElementById('Cuadrante5')
    let recorte5ctx = recorte5.getContext('2d')

    recorte5ctx.strokeStyle = "#ff0000";
    recorte5ctx.lineWidth = 4;
    recorte5ctx.strokeRect(0, 0, 220, 140)
}

function cuad6red(){
    let recorte6= document.getElementById('Cuadrante6')
    let recorte6ctx = recorte6.getContext('2d')

    recorte6ctx.strokeStyle = "#ff0000";
    recorte6ctx.lineWidth = 4;
    recorte6ctx.strokeRect(0, 0, 220, 140)
}

function cuad7red(){
    let recorte7= document.getElementById('Cuadrante7')
    let recorte7ctx = recorte7.getContext('2d')

    recorte7ctx.strokeStyle = "#ff0000";
    recorte7ctx.lineWidth = 4;
    recorte7ctx.strokeRect(0, 0, 220, 140)
}


//************************************************************************************** Funciones de arduino*/

async function arduino(point,ip,port){

if(point == 1){ callardu(point,ip,port)}
if(point == 2){ callardu(point,ip,port)}
if(point == 3){ callardu(point,ip,port)}
if(point == 4){ callardu(point,ip,port)}
if(point == 5){ callardu(point,ip,port)}
if(point == 6){ callardu(point,ip,port)}
if(point == 7){ callardu(point,ip,port)}
if(point == 8){ callardu(point,ip,port)}

}

//****************************************** Backend call functions


function  callardu(p,ip,port){

	const socket = io();		
	socket.emit('callardu',p,ip,port);		
}

//************************************************************************************** Funciones para localizar los Arrays*/
function allpoints(pot,statusl){ 
    boxpoint[pot] = statusl// Array guarda el valor de cada punto analizado 
}

function evaluaArray(){
    let evalua 
    return new Promise(async resolve =>{ 
    //Cuadrante 1
    desicion[0]=boxpoint[1]
    desicion[1]=boxpoint[2]
    desicion[2]=boxpoint[3]
    desicion[3]=boxpoint[4]
    evalua = desicion.some((e) => e == "0") // Linea de codigo funciona para encontrar algo diferente en la cadena 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    console.log("Desicion final de evalua:",evalua)
    pointstatus(1,evalua) // Se manda a allmar funcion una ves que haya evaluado todos los puntos que inspeccione en el cuadrante 
    desicion=[] //Limpia matriz de trabajo

     //Cuadrante 2
    desicion[0]=boxpoint[5]
    desicion[1]=boxpoint[6]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    pointstatus(2,evalua)
    desicion=[]

     //Cuadrante 3
    desicion[0]=boxpoint[7]
    desicion[1]=boxpoint[8]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"}
    pointstatus(3,evalua)
    desicion=[]

     //Cuadrante 4
    desicion[0]=boxpoint[9]
    desicion[1]=boxpoint[10]
    desicion[2]=boxpoint[11]
    desicion[3]=boxpoint[12]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    pointstatus(4,evalua)
    desicion=[]

    //Cuadrante 5
    desicion[0]=boxpoint[13]
    desicion[1]=boxpoint[14]
    desicion[2]=boxpoint[15]
    desicion[3]=boxpoint[16]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    pointstatus(5,evalua)
    desicion=[]

    //Cuadrante 6
    desicion[0]=boxpoint[17]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    pointstatus(6,evalua)
    desicion=[]

    //Cuadrante 7
    desicion[0]=boxpoint[18]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 
    pointstatus(7,evalua)
    desicion=[]

    let resultadofinal = boxpoint.some((e) => e == "0")
    if (resultadofinal== false){
        pass() // funciones que van integradas en Sweet alert 
    }else{
        fail()
    }
    resultado = resultadofinal
    console.log(boxpoint)
    resolve('resolved')}) // fin de promesa
}

//************************************************************************************** Funciones de debug*/
function canbughide(){ // funcion para esconder los canvas 
    return new Promise(async resolve =>{  
    document.getElementById('CanvasFHD').style.visibility = "hidden"
    document.getElementById('Canvascut').style.visibility = "hidden"
    document.getElementById('tesser').style.visibility = "hidden"
    resolve('resolved')});
}
function canbugshow(){ // funcion para ver los canvas 
    document.getElementById( 'Canvascut' ).style.visibility = "visible"
    document.getElementById('CanvasFHD').style.visibility = "visible"
    document.getElementById('tesser').style.visibility = "visible"
    document.getElementById('tesser2').style.visibility = "visible"
}

//************************************************************************************** Funciones de camara*/
/*Seccion de camaras  */
async function open_cam(){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    camid="b018bdec8ce28e5652c262616b9fd067e9144dacf6c13ded795774553e2e7478"// camara 1

    const video = document.querySelector('video');
     const vgaConstraints = {
           
        video: {deviceId: camid}, //width: {exact: 280}, height: {exact: 280} / deviceId: "5bba2c7c9238e1d8ab5e90e2f2f94aa226749826319f6c705c5bfb5a3d2d5279"
                 width: { ideal: 1920 },
                 height: { ideal: 1080 }
        };

         await navigator.mediaDevices.getUserMedia(vgaConstraints).
        then((stream) => {video.srcObject = stream});
        setTimeout(function fire(){resolve('resolved');},2000);
    });//Cierra Promise principal
}
async function captureimage(){// Resolve de 2 segundos
        return new Promise(async resolve =>{        
            
           let image = document.getElementById( 'CanvasFHD' );
           let contexim2 = image.getContext( '2d' );	

           var video = document.getElementById("video");     
           
           w = image.width;
           h = image.height;
            
           contexim2.drawImage(video,0,0,image.width,image.height);
            //var dataURI = canvas.toDataURL('image/jpeg');
        	setTimeout(function fire(){resolve('resolved');},2300);//Temporal para programacion de secuencia
        	resolve('resolved')});
}
function mapcams(){ // Mapeo de camaras para identificarlas 
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}
function stopcam(){
    return new Promise(async resolve =>{
        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        //console.log(tracks);
        // Tracks are returned as an array, so if you know you only have one, you can stop it with: 
        //tracks[0].stop();
        // Or stop all like so:
        tracks.forEach(track => {track.stop()})//;console.log(track);
    setTimeout(function fire(){resolve('resolved');},1000);
    });//Cierra Promise principal
}
async function snapshot(){//Back end sent URI,SN? & point?
	return new Promise(async resolve =>{
		var dataURI = fullimage.toDataURL('image/jpeg');
		savepic(dataURI,contenido,point); //savepic(dataURI,point);
		//console.log("Pic Sent--"+sn+"--"+point);
		//setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
		resolve('resolved')});
}	

function savepic(uri,contenido,point){
    // let serialnumber="Unit_under_test" 
     //let point = 2           
     const socket = io();
     socket.emit('picsaving',uri,contenido,point);	
     //console.log(contenido)
 }

function logsaving(contenido,logdata){
    //return new Promise(async resolve =>{
    const socket = io();
    socket.emit('logsaving',contenido,logdata);
   // setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
   // });
}

function response(boxpoint){
    return new Promise(async resolve =>{
    //console.log(IAdesition)
    boxpoint = 
    ""+contenido+"\n\nTX01 = " + `IA Inspection : ${IAdesition[1] == 0 ? 'Fail' :'Pass' } ` + "\n\n" + 
    "IN1 = " + `IA Inspection : ${IAdesition[2] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "TX02 = " + `IA Inspection : ${IAdesition[3] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "IN2 = " + `IA Inspection : ${IAdesition[4] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "DO1 = " + `IA Inspection : ${IAdesition[5] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "DO2 = " + `IA Inspection : ${IAdesition[6] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "TXI1 = " + `IA Inspection : ${IAdesition[7] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "TXI2 = " + `IA Inspection : ${IAdesition[8] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "LA = " + `IA Inspection : ${IAdesition[9] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "LB = " + `IA Inspection : ${IAdesition[10] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "TX1 = " + `IA Inspection : ${IAdesition[11] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "TX2 = " + `IA Inspection : ${IAdesition[12] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +    
    "LO1 = " + `IA Inspection : ${IAdesition[13] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "LO2 = " + `IA Inspection : ${IAdesition[14] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "DI1 = " + `IA Inspection : ${IAdesition[15] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "DI2 = " + `IA Inspection : ${IAdesition[16] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "OFE1 = " + `IA Inspection : ${IAdesition[17] == 0 ? 'Fail' :'Pass' } ` + "\n\n" +
    "OFE2 = " + `IA Inspection : ${IAdesition[18] == 0 ? 'Fail' :'Pass' } ` + "\n\n" 

    logsaving(contenido,boxpoint,resultado)
    resolve('resolved')});
}

//************************************************************************************** IA*/
const classifier = knnClassifier.create() //Contiene librerias para poder funcionar 

ml()

async function mlinspector(pot){
    return new Promise(async resolve =>{ // inicio de promesa 
        let x = 0
        let y = 0
        let w = 0
        let h = 0

        if(pot == 1){x = 146, y = 208, w = 316, h = 857} // Referencia de coordenadas a inspeccionar
        if(pot == 2){x = 468, y = 208, w = 316, h = 857} 
        if(pot == 3){x = 1195, y = 178, w = 316, h = 857} 
        if(pot == 4){x = 1521, y = 178, w = 316, h = 857} 

        if(pot == 5){x = 299, y = 145, w = 316, h = 857} 

        if(pot == 6){x = 624, y = 100, w = 316, h = 857} 

        if(pot == 7){x = 291, y = 131, w = 316, h = 857}

        if(pot == 8){x = 621, y = 133, w = 316, h = 857}
        if(pot == 9){x = 39, y = 10, w = 1023, h = 228}
        if(pot == 10){x = 59, y = 290, w = 1023, h = 228}
        if(pot == 11){x = 70, y = 539, w = 1023, h = 228}
        if(pot == 12){x = 67, y = 774, w = 1023, h = 228}
        if(pot == 13){x = 13, y =10, w = 1023, h = 228}
        if(pot == 14){x = 18, y =254, w = 1023, h = 228}
        if(pot == 15){x = 18, y = 500, w = 1023, h = 228}
        if(pot == 16){x = 18, y = 749, w = 1023, h = 228}
        if(pot == 17){x = 0, y =0, w = 951, h = 253}
        if(pot == 18){x = 0, y =0, w = 951, h = 253}


        switch(pot){
            // Inicia cuadrante 1
            case 1: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TX01",23) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[1]= statusx
            break

            case 2: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("IN1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[2]= statusx
            break

            case 3: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TX02",55) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[3]= statusx
            break

            case 4: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("IN2",45) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[4]= statusx
            break

             // Inicia cuadrante 2
            case 5: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("DO1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[5]= statusx
            break

            case 6: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("DO2",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[6]= statusx
            break 

            // Inicia cuadrante 3
            case 7: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TXI1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[7]= statusx
            break 

            case 8: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TXI2",20) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[8]= statusx
            break 

            case 9: 
            horicortitoctx.drawImage(fullimage,x,y,w,h,0,0,horicortitoctx.canvas.width,horicortitoctx.canvas.height)
            await call("LA",22) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[9]= statusx
            break 

            case 10: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("LB",31) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[10]= statusx
            break 

            case 11: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TX1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[11]= statusx
            break 

            case 12: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("TX2",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[12]= statusx
            break 

            case 13: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("LO1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[13]= statusx
            break 

            case 14: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("LO2",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[14]= statusx
            break 
            case 15: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("DI1",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[15]= statusx
            break 
            case 16: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("DI2",33) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[16]= statusx
            break 
            case 17: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",31) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[17]= statusx
            break 
            case 18: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",31) // Argumentos nuca y vari
            await predict(recortito)
            IAdesition[18]= statusx
            break 
            default:

        }
    resolve('resolved')})  
}
async function ml() {
    // Load the model.
    net = await mobilenet.load(); // red neuronal echa, ejemplo, ya esta entrenada  
    console.log('Neural network load success...');
}

/************************ Variable 'nuca' representa el nombre del documento JSON variable */
/************************ Variable 'vari' representa numero de muestras de la red neuronal*/
function call(nuca,vari){ 
    return new Promise(async resolve =>{
      let data;
      load("/ml/neuralnetworks/"+nuca+".json", function(text){ //C:/Users/juan_moreno/myapp/public/Nokia Vision
      data = JSON.parse(text);	
      //*****************************COnvertir a tensor
      //Set classifier
      Object.keys(data).forEach((key) => {
        data[key] = tf.tensor2d(data[key],[vari,1024]); //Shape es [# imagenes,dimension pix] ,[19,1024]
      });
      //Set classifier
      //console.log(data);
      classifier.setClassifierDataset(data)
           }) // fin de load 
           setTimeout(function fire(){resolve('resolved')},500); 
       }) // fin de promesa 
}
function load(file, callback) {  // funcion que se encarga de llamar Json con el nuevo entrenamiento 
        //Can be change to other source	
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
               callback(rawFile.responseText);
        }
     }
    rawFile.send(null)
}
async function predict(){ // wonka es variable
        return new Promise(async resolve =>{	
        // Get the activation from mobilenet from the webcam.
        const activation = net.infer(recortito, 'conv_preds');
        // Get the most likely class and confidences from the classifier module.
        const result = await classifier.predictClass(activation); // Clasifica, decide depende de la imagen lo que le va a poner 
        const classes = ['Pass', 'Fail'];
        
        if(classes[result.label] == 'Pass'){
            statusx = "1" 
            console.log("AI Inspection: " + `${statusx == 1 ? 'Pass' :'Fail' }`)
        }else{
            statusx = "0"
            console.log("AI Inspection: " + `${statusx == 0 ? 'Fail' :'Pass' }` )
        }
            //console.log(classes[result.label])
           // console.log(result.confidences[result.label]*100)
        resolve('resolved')
        });//Cierra Promise						
}//if logic end






