

#define DIRx 5  //Este pin controla la direccion del giro del motor "X" .
#define STEPx 4 //Se declara el pin de salida de los pulsos de pasos al driver "X".
#define ENEx 3 //Este pin habilita o deshabilita el motor "X" mediante el driver .

#define DIRy 2  //Este pin controla la direccion del giro del motor "Y".
#define STEPy 1 //Se declara el pin de salida de los pulsos de pasos al driver "Y".
#define ENEy 0 //Este pin habilita o deshabilita el motor "Y" mediante el driver .

#define SENx 9 //Pin de fin e carrera en eje X.
#define SENy 2 //Pin de fin e carrera en eje Y.


#include <Ethernet.h>

  int stepsx = 0;
  int stepsy = 0;
  
//Introduzca una direccion MAC y la direccion IP para el controlador
byte mac[] = { 
0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(172,24,139,2);   // Esta direccion IP debe ser cambiada obligatoriamente 
                             
EthernetServer miservidor(80);    // Puerto 80 por defecto para HTTP 
EthernetClient micliente;
String readString;
                   
void setup() {
int stateX= analogRead(1); /* Fin de carrera en X */
int stateY= analogRead(0); /* Fin de carrera en Y */

 pinMode (SENx, INPUT);
 pinMode (SENy, INPUT);

 pinMode (STEPx, OUTPUT);
 pinMode (DIRx, OUTPUT);
 pinMode (ENEx, OUTPUT);

 pinMode (STEPy, OUTPUT);
 pinMode (DIRy, OUTPUT);
 pinMode (ENEy, OUTPUT);

 Ethernet.begin(mac, ip);    //inicializa la conexion Ethernet y el servidor
  miservidor.begin();
 
}

void loop() {
int  stateX= 0; /* Fin de carrera en X */
int  stateY= 0; /* Fin de carrera en Y */

 int input;
  micliente = miservidor.available();
  
  if(micliente)
  {
     String bufferString = "";
     

     while (miservidor.available()) {
      bufferString += (char)micliente.read();
    }
    
    // miservidor.write(input);
Serial.println(bufferString);
input = (bufferString.toInt());

switch(input){ 

case 0:
   Serial.println("case 0");
break;
case 1:
//PASOS EN X
digitalWrite(ENEx, HIGH);

digitalWrite(ENEx, LOW);
digitalWrite(DIRx, HIGH ); // HIGH => Movimiento partiendo de Home, LOW => regreso a home        
          for (int i = 0; i < 3000; i++) // Pasos para llegar a punto1 en X => 1544 
          {
          digitalWrite(STEPx, HIGH);
          delayMicroseconds (90);  
          digitalWrite(STEPx, LOW);
          delayMicroseconds (90);
          }
          digitalWrite(ENEx, HIGH);
          delay(1000);  

//PASOS EN Y
digitalWrite(ENEy, LOW);
digitalWrite(DIRy, LOW ); // HIGH => Movimiento partiendo de Home, LOW => regreso a home    
               for (int i = 0; i <2000 ; i++) // Pasos para llegar a punto1 en X => 1544 
               {
               digitalWrite(STEPy, HIGH);
               delayMicroseconds (140);  
               digitalWrite(STEPy, LOW);
               delayMicroseconds (140);
               }
               digitalWrite(ENEy, HIGH);
               delay(1000);    
               micliente.write("En posicion");
break;
case 2:  
//PASOS EN X 
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, HIGH );                         // HIGH => Movimiento partiendo de Home, LOW => regreso a home
          //pointX2 = pointX1 - 8067;
                for (int i = 0; i < 2392; i++)   // Pasos para llegar a punto2 en X => 2392
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
              
//PASOS EN Y
digitalWrite(ENEy, LOW);
digitalWrite(DIRy, HIGH);                       // HIGH parte movimiento de home, low regreso a home               
                for (int j = 0; j < 0; j++)     // Pasos para llegar a punto1 en Y => 0
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);  
                micliente.write("En posicion");
break;
case 3:
//PASOS EN X 
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, HIGH );                      // HIGH => Movimiento partiendo de Home, LOW => regreso a home
                for (int i = 0; i < 1755; i++) // Pasos para llegar a punto1 en X => 1544 
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
//PASOS EN Y
digitalWrite(ENEy, LOW);                
digitalWrite(DIRy, HIGH);                      // HIGH parte movimiento de home, low regreso a home               
                 for (int j = 0; j < 0; j++)   // Pasos para llegar a punto1 en Y => 0 
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);  
                 micliente.write("En posicion");
break;
case 4:
//PASOS EN X
digitalWrite(ENEx, LOW); 
digitalWrite(DIRx, HIGH);
                for (int i = 0; i <3800; i++) // Pasos para llegar a punto1 en X =>  11CM
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
//PASOS EN Y                
digitalWrite(ENEy, LOW);
digitalWrite(DIRy, LOW);                    // HIGH parte movimiento de home, low regreso a home               
                for (int j = 0; j < 2900; j++) // Pasos para llegar a punto1 en Y =>  3397
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);
                micliente.write("En posicion");
break;
case 5:
//PASOS EN X
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, HIGH );                  // HIGH => Movimiento partiendo de Home, LOW => regreso a home
                for (int i = 0; i < 0; i++) // Pasos para llegar a punto1 en X => 
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
//PASOS EN Y           
digitalWrite(ENEy, LOW);
digitalWrite(DIRy, LOW);                // HIGH parte movimiento de home, low regreso a home               
                for (int j = 0; j < 1776; j++) // Pasos para llegar a punto1 en Y => 1776, 11.5CM 
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);  
                 micliente.write("En posicion");
break;
case 6:
//PASOS EN X
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, LOW );                     // HIGH => Movimiento partiendo de Home, LOW => regreso a home
                for (int i = 0; i < 4000; i++) // Pasos para llegar a punto1 en X => 
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
//PASOS EN Y                 
digitalWrite(ENEy, LOW); 
digitalWrite(DIRy, HIGH);                    // HIGH parte movimiento de home, low regreso a home               
                for (int j = 0; j < 0; j++) // Pasos para llegar a punto1 en Y => 1776, 11.5CM 
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);  
                micliente.write("En posicion");
break;
case 7:
//PASOS EN X
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, LOW );                     // HIGH => Movimiento partiendo de Home, LOW => regreso a home
                for (int i = 0; i < 900; i++) // Pasos para llegar a punto1 en X => 
                {
                digitalWrite(STEPx, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPx, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEx, HIGH);
                delay(1000);
//PASOS EN Y                 
digitalWrite(ENEy, LOW); 
digitalWrite(DIRy, HIGH);                    // HIGH parte movimiento de home, low regreso a home               
                for (int j = 0; j < 0; j++) // Pasos para llegar a punto1 en Y => 
                {
                digitalWrite(STEPy, HIGH);
                delayMicroseconds (90);  
                digitalWrite(STEPy, LOW);
                delayMicroseconds (90);
                }
                digitalWrite(ENEy, HIGH);
                delay(1000);  
                micliente.write("En posicion");

break;
case 8:
// Home X
// 0 = sensor en X 
digitalWrite(ENEx, LOW);
digitalWrite(DIRx, LOW );   

int motx = analogRead(0); // Se declara y se lee el estado del sensor 

// El valor inicial es FF -> Desactivado 
 while(motx != 0){ 
       digitalWrite(STEPx, HIGH);
       delayMicroseconds (90);
       digitalWrite(STEPx, LOW);
       delayMicroseconds (90);
       
       motx = analogRead(0); // Leer el valor actual del estado del sensor 
       }
       
// Home Y
// 1 = sensor en Y
digitalWrite(ENEy, LOW);
digitalWrite(DIRy, HIGH ); 

int moty = analogRead(1);

   while(moty != 0){ 
       digitalWrite(STEPy, HIGH);
       delayMicroseconds (90);
       digitalWrite(STEPy, LOW);
       delayMicroseconds (90);

       moty = analogRead(1); // Leer el valor actual del estado del sensor 
       }    
       micliente.write("En posicion");                                       
break;
   }/*Fin de switch   */
 }/*ciere de if  */
}/* CIERRE DE LOOP */    

 
    
 
